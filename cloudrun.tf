#cloud run

resource "google_cloud_run_service" "default" {
  name     = "hello"
  location = "us-central1"
  project  = "splendid-parsec-356405"

  template {
    spec {
      containers {
        image = "gcr.io/cloudrun/hello"
      }
    }
  }
}

resource "google_cloud_run_service_iam_member" "member" {
  location = google_cloud_run_service.default.location
  project  = google_cloud_run_service.default.project
  service  = google_cloud_run_service.default.name
  role     = "roles/run.invoker"
  member   = "allUsers"
}

# VPC
resource "google_compute_network" "default" {
  name                    = "cloud-run-vpc-network"
  auto_create_subnetworks = false
}

# backend subnet

resource "google_compute_subnetwork" "default" {
  name          = "cloud-run-subnet"
  ip_cidr_range = "10.0.1.0/24"
  region        = "us-central1"
  network       = google_compute_network.default.id
}

# reserve IP address

resource "google_compute_global_address" "default" {
  name     = "static-ip"
}

# create a managed SSL certificate that's issued and renewed by Google

resource "google_compute_managed_ssl_certificate" "default" {

  name = "j-ssl-certificate"
  managed {
    domains = ["lforlearn.online"]
  }
}

#make a network endpoint group (NEG) out of your serverless service

resource "google_compute_region_network_endpoint_group" "cloudrun_neg" {
  name                  = "cloud-run-serverless-neg"
  network_endpoint_type = "SERVERLESS"
  region                = "us-central1"
  cloud_run {
    service = google_cloud_run_service.default.name
  }
}

#create a backend service that'll keep track of these network endpoints

resource "google_compute_backend_service" "default" {
  name      = "cloud-run-lb-backend"

  protocol  = "HTTP"
  port_name = "http"
  timeout_sec = 30
  enable_cdn  = true
  
  backend {
    group = google_compute_region_network_endpoint_group.cloudrun_neg.id
  }
}

#create an empty URL map that doesn't have any routing rules and sends the traffic to this backend service

resource "google_compute_url_map" "default" {
  name            = "lb-urlmap"
  default_service = google_compute_backend_service.default.id
}

#configure an HTTPS proxy to terminate the traffic with the Google-managed certificate and route it to the URL map

resource "google_compute_target_https_proxy" "default" {
  name   = "lb-https-proxy"

  url_map          = google_compute_url_map.default.id
  ssl_certificates = [
    google_compute_managed_ssl_certificate.default.id
  ]
}

#configure a global forwarding rule to route the HTTPS traffic on the IP address to the target HTTPS proxy

resource "google_compute_global_forwarding_rule" "default" {
  name   = "frwd-rule-lb"
  target = google_compute_target_https_proxy.default.id
  port_range = "443"
  ip_address = google_compute_global_address.default.address
}

#create an output variable that lists your IP address

output "cloud_run_url" {
  value = element(google_cloud_run_service.default.status, 0).url
}

output "load_balancer_ip" {
  value = google_compute_global_address.default.address
}
